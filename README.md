Dear reviewers,

This is the replication package for our ICSE 2018 submitted paper: "How Practitioners Perceive Coding Proficiency". 

In the package, we have:

1. Appendix of the paper: `appendix.pdf`

2. Coding process for the rationales:  `Coding Process.pdf`

3. Survey and Interview Questions: `Developer Coding Proficiency.pdf`

4. Raw Data from the survey participants: `Raw Data-Global.pdf` and `Raw Data-Chinese.pdf`.  (Note that we have a Chinese version of our survey.)

5. Scores for each skill: `Results.xlxs`

6. Data to reproduce the results in Table II, Table IV - VII (in Appendix) of the paper: `ImportantTask.R`(`ranking.txt`) for Table II and `experience.R`(`exp.txt`) for Table IV - VII


Thank you so much !